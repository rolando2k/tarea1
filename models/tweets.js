const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const tweetsSchema = new mongoose.Schema(
    {
        _id: new Schema.Types.ObjectId,
        userId: {
            type: ObjectId,
            ref: "Users",
            required: true
        },    
        message: {
            type: String,
            trim: true,
            default: []
        }
    },
    { 
        timestamps: true 
    }
);

/*
_id: automatic
userId: ObjectId
message: String

*/

module.exports = mongoose.model("Tweets", tweetsSchema);


