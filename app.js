require("dotenv").config();

// generic imports
const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const users = require("./models/users");
const uuidv1 = require('uuid/v1');



// app - express
const app = express();

// connection
const db = async () => {
    try {
        const success = await mongoose.connect(process.env.DATABASE, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true, 
            useFindAndModify: false
        });

        console.log('DB Connected '+process.env.DATABASE);
    } catch (error) {
        console.log('DB Connection Error', error);
    }
}

// db connection
db();


// middlewares
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cors());


// puerto
const port = process.env.PORT || 8000;

// listen puerto
app.listen(port, () => {
    console.log(`Server is running on ports ${port}`);
});